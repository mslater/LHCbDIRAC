# Job Execution Rate Overview
The `Job Execution Rate Overview` dashboard can be found in the [Shifter Overview](https://monit-grafana.cern.ch/d/H0xLN8A4k/job-execution-rate-overview?orgId=46) folder of the LHCb grafana organisation.
Plots are organised by three 'rows' (`Tier 0/1`, `Tier 2`, `Tier 2-D`).
Each row corresponds to a hidden variable that lists the sites for that tier (variables are also in the json).
- For tier 2, This list is not yet final as it seems more complex than 'not tier 1 sites'. The difference between grafana and DIRAC is O(100 jobs) per timestamp.

## Tier 0/1
- Job Execution by FinalMinorStatus (AccountingDB)
- Running Jobs by JobSplitType (WMS indices; not yet validated)
- Running Jobs by Site (WMS indices; not yet validated)

## Tier 2
- Job Execution by FinalMinorStatus (AccountingDB)
- Running Jobs by JobSplitType (WMS indices; not yet validated)
- Running Jobs by Site (WMS indices; not yet validated)

## Tier 2-D
- Running User Jobs by User (WMS indices; not yet validated)
- Running User Jobs by JobSplitType (WMS indices; not yet validated)
- Running User Jobs by Site (WMS indices; not yet validated)
