# Pilot Jobs Status
The `Pilot Jobs Status` dashboard can be found in the [Shifter Overview](https://monit-grafana.cern.ch/d/Hfdgmcx4z/pilot-jobs-status?orgId=46) folder of the LHCb grafana organisation.
Plots for tier 2 depend on a query to AccountingDB to list all the tier 2 sites. This list is not yet final as it seems more complex than 'not tier 1 sites'. The difference between grafana and DIRAC is O(100 jobs) per timestamp.

## Pilot Status / T1
Using AccountingDB as datasource

## Pilot Status / T2
Using AccountingDB as datasource

Not yet properly validated due to issue with tier 2 (above)

## Aborted Pilots by Site
Using AccountingDB as datasource

Not yet properly validated due to issue with tier 2 (above)

## Failed Pilots by Site
Using AccountingDB as datasource

Not yet properly validated due to issue with tier 2 (above)
